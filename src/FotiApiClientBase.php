<?php

/**
 * Base library Foti API V2
 *
 * @version 1.16
 */
 
namespace Foticos\FotiApiClient;

use Exception; 


class FotiApiClientBase
{
	const VERSION 				= '1.14';
	const PATH_AUTH 			= 'auth/';
	const DEFAULT_API_URL 		= 'https://api.foticos.com:8443/v2/';
	
	const CART_TYPE_CUSTOMIZED 	= "customized";
	const CART_TYPE_MATERIAL 	= "material";

	// IMPORTANTE!!! tiene que ser así las claves del array, no poner entre comillas
	private $CURL_OPTS = array(
		CURLOPT_CONNECTTIMEOUT => 10,
		CURLOPT_RETURNTRANSFER => 1,
		CURLOPT_TIMEOUT        => 60,
		CURLOPT_USERAGENT      => 'PHP FotiApiClient 1.11',
		CURLOPT_SSL_VERIFYPEER => 0		
	);
	
	protected $appId;
	protected $secretKey;
	protected $accessToken;
	protected $publicToken;
	protected $apiUrl = self::DEFAULT_API_URL;
	protected $lastQueryInfo = [];
	protected $uuid;
	
	// prop_name, cfg_name, default_value, mandatory
	const CONFIGURABLE_BASECONFIG = array(
		'app_id'       => 'appId',
		'secret_key'   => 'secretKey',
		'access_token' => 'accessToken',
		'public_token' => 'publicToken',
		'url'          => 'apiUrl',
		'uuid'         => 'uuid'
	);

	public function __construct($config)
	{
		!empty($config) && $this->configBase($config);
	}	

	public function configBase($config)
	{				
		foreach ($config as $key=>$value)
		{
			if (array_key_exists($key, static::CONFIGURABLE_BASECONFIG) && property_exists($this, static::CONFIGURABLE_BASECONFIG[$key]))
				$this->{static::CONFIGURABLE_BASECONFIG[$key]} = $value;
		}
	}

	public function api($path, $method, $parameters = array(), $multipart = false)
	{
		$method = strtoupper($method);

		if ($this->getAccessToken() && !array_key_exists('access_token', $parameters))
			$parameters['access_token'] = $this->getAccessToken();				
		
		$result = $this->processApiResponse($this->makeRequest($path, $method, $parameters, $multipart));

		return $result;
	}

	protected function processApiResponse($response)
	{		
		if (!empty($response))
		{			
			$output = json_decode($response, true);
							
			if ((json_last_error() == JSON_ERROR_NONE))
			{
				$this->lastQueryInfo['uuid'] = $output['uuid'] ?? null;

				if (array_key_exists('error', $output))
				{
					$output = $this->apiError(
						$output['error']['error_code'],
						$output['error']['message'],
						$output['error']['description'],
						$output['error']['validation'] ?? null,
						$output['error']['uuid'] ?? null
					);
				}				
			}
			else
			{
				// NON JSON FORMAT
				$output = $response;
			}
		}
		else
		{
			throw new Exception('Unexpected empty data from API (not catched error or response not defined)');
		}

		if (is_array($output) && key_exists('error_code', $output) && $output['error_code'] != 200)
			$this->throwApiResponseError($output);

		return $output;
	}

	private function apiError($error_code, $message, $description = null, $validation = null, $uuid = null)
	{
		return array(
			'error_code'  => $error_code,
			'message'     => $message,
			'description' => $description,
			'validation'  => $validation,
			'uuid'        => $uuid
		);
	}

	private function throwApiResponseError($result){		
		$description = key_exists('description', $result) ? $result['description'] : null;
		$validation = key_exists('validation', $result) ? $result['validation'] : null;
		$uuid = key_exists('uuid', $result) ? $result['uuid'] : null;
		//dd($this->lastQueryInfo);
		throw new FotiApiException($result['error_code'], $result['message'], $description, $validation, $uuid);		
	}


	public function getAccessToken()
	{
		return $this->accessToken;
	}

	public function setAccessToken($accessToken)
	{
		$this->accessToken = $accessToken;
	}

	public function getPublicToken()
	{
		return $this->publicToken;
	}

	public function setPublicToken($publicToken)
	{
		$this->publicToken = $publicToken;
	}

	public function getAppId()
	{
		return $this->appId;
	}

	public function setAppId($id)
	{
		$this->appId = $id;
	}

	public function getSecretKey()
	{
		return $this->secretKey;
	}	

	protected function makeRequest($path, $method = 'GET', $parameters = array(), $multipart = false)
	{
		$ch = curl_init();

		curl_setopt_array($ch, $this->CURL_OPTS);	

		if ($method == 'GET')
		{
			if (is_array($parameters) && count($parameters) > 0)
			{
				foreach ($parameters as $key => $value){
					if (is_null($value))
						$parameters[$key] = ''; 		            	
				}				 
			}

			$path .= ((strpos($path, '?') === false) ? '?' : '&') . http_build_query($parameters);
		}

		curl_setopt($ch, CURLOPT_URL, $this->apiUrl . $path);

		if ($method == 'POST' || $method == 'DELETE')
		{
			if (!$multipart)			
			{
				$stringPost = ''; 

		        if (is_array($parameters) && count($parameters) > 0)
		        {
		          	foreach ($parameters as $key => $value){ 
		            	if (is_null($value))
		              		$parameters[$key] = ''; 		            	
		          	} 
		          	$stringPost = http_build_query($parameters); 
		        }		        

		        $parameters = $stringPost;
			}			
		}

		if ($method == 'GET')
		{
			curl_setopt($ch, CURLOPT_HTTPGET, true);
			curl_setopt($ch, CURLOPT_POST, false);
		}
		elseif ($method == 'POST')
		{
			curl_setopt($ch, CURLOPT_POST, true);					
			curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
		}
		else if ($method == 'DELETE')
		{
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
		}

		if (!empty($this->uuid))
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, [
				'uuid: '.$this->uuid
			]);
		}

		/* 
		// to get headers from the response (x-hit, etc)
		$headers = [];

		curl_setopt($resURL, CURLOPT_HEADERFUNCTION, function($resURL, $strHeader) use (&$headers){
			$len    = strlen($header);
			$header = explode(':', $header, 2);
			if (count($header) < 2) // ignore invalid headers
			return $len;

			$headers[strtolower(trim($header[0]))][] = trim($header[1]);

			return $len;
		}); 
		*/

		$this->setLastQueryInfo($method, $this->apiUrl . $path, $parameters, ['multipart'=>$multipart]);

		$rawOutput = curl_exec($ch);		

		if (!$rawOutput)
			throw new Exception('Error executing curl '.$method.' '.$this->apiUrl . $path.' ==> ' .curl_error($ch));

		$info = curl_getinfo($ch);		

		curl_close($ch);

		$output = null;

		if (in_array($info['http_code'], [0, 404]))
			throw new Exception('Could not establish connection to server');
		else
			$output = $rawOutput;
		
		if (strlen($output) < 10000)
			$this->lastQueryInfo['result'] = $output;
		else
			$this->lastQueryInfo['result'] = 'too long result to display';		

		return $output;
	}

	public function getLastQuery(){
		return $this->lastQueryInfo;
	}

	public function setLastQueryInfo($method, $url, $parameters, $extra = [])
	{
		$extra['multipart'] = $extra['multipart'] ?? false;
		$extra['cache_hit'] = $extra['cache_hit'] ?? false;

		$this->lastQueryInfo = array_merge(array(
				'method'     => $method,
				'url'        => $url,
				'parameters' => $parameters
			), $extra
		);
	}
	
	private static function isJson($string)
	{
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}
	
}