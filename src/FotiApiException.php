<?php

namespace Foticos\FotiApiClient;

class FotiApiException extends \Exception{

	protected $uuid;

	public function __construct($code, $message, $description = null, $validation = null, $uuid = null) {
		$this->description = $description;
		$this->validation = $validation;
		$this->uuid = $uuid;
		parent::__construct($message, $code);
	}

	public function getUuid(){
		return $this->uuid;
	}
}