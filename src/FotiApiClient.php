<?php

/**
 * Overloads FotiApiClientBase to manage token through cookies and exceptions
 *
 * - Stores the access_token(crypted), public_token and ext_session_id in a cookie at the user browser
 * - Generates token automatically (if not provided)
 * - Auto renews token automatically when it is expired
 *
 * @version 1.16
 */
 
namespace Foticos\FotiApiClient;

use Exception;

class FotiApiClient extends FotiApiClientBase
{
	const DEFAULT_COOKIE_NAME = 'FotiApiClientV2';
	const DEFAULT_COOKIE_EXPIRATION = 2629743; // 1 Month
	const DEFAULT_COOKIE_SECURE = false;
	const DEFAULT_COOKIE_HTTPONLY = true;
	// Throw exception on errors or return errors as an array
	const DEFAULT_COOKIE_PATH = '/';
	const DEFAULT_DISABLE_COOKIE = false;
	
	const CRYPT_ALGORITM = 'AES-256-CFB';	
	
// 	private static $PATHS_POST_MULTIPART = array('mails');
	
	// Try to regenerate token automatically with the following error codes
	private $regenerationErrorCodes = array(2010, 2011, 2014);
	private $cookieName             = self::DEFAULT_COOKIE_NAME;
	private $cookieExpiration       = self::DEFAULT_COOKIE_EXPIRATION;
	private $cookiePath             = self::DEFAULT_COOKIE_PATH;
	private $cookieDomain           = null;
	private $cookieSecure           = self::DEFAULT_COOKIE_SECURE;
	private $cookieHttponly         = self::DEFAULT_COOKIE_HTTPONLY;
	private $extSessionId           = null;
	private $disableCookie          = self::DEFAULT_DISABLE_COOKIE;
	private $autogenerateTokens     = true;
	private $eventHandler;
	
	// prop_name, cfg_name, default_value, mandatory
	const CONFIGURABLE_CONFIG = array(
		'cookie_name'         => 'cookieName',
		'cookie_expiration'   => 'cookieExpiration',
		'cookie_path'         => 'cookiePath',
		'cookie_domain'       => 'cookieDomain',
		'cookie_secure'       => 'cookieSecure',
		'cookie_httponly'     => 'cookieHttponly',
		'ext_session_id'      => 'extSessionId',
		'disable_cookie'      => 'disableCookie',
		'autogenerate_tokens' => 'autogenerateTokens'
	);

	private $config = [];
		
	public function __construct($config = [])
	{		
		!empty($config) && $this->config($config);
	}

	public function config($config)
	{
		parent::__construct($config);

		foreach ($config as $key=>$value)
		{
			if (array_key_exists($key, static::CONFIGURABLE_CONFIG) && property_exists($this, static::CONFIGURABLE_CONFIG[$key]))
				$this->{static::CONFIGURABLE_CONFIG[$key]} = $value;
		}		
				 
		$this->config = array_merge($this->config, $config);

		if (!isset($this->appId))
			return;

		if (!$this->disableCookie)
			$this->fillDataFromCookie($this->autogenerateTokens);

		if (($this->initExtSessionId() || $this->initTokens()) && !$this->disableCookie)
			$this->saveCookie();		
	}

	private function initExtSessionId()
	{
		if (empty($this->getExtSessionId()))
		{
			$this->setExtSessionId(md5(uniqid(rand(), true)));
			return true;
		}

		return false;
	}

	private function initTokens()
	{
		if (empty($this->getPublicToken()) || empty($this->getAccessToken()) && $this->autogenerateTokens)
		{			
			$this->generateNewToken();
			// already saved in this->generateNewToken
			return true;
		}

		return false;
	}

	public function api($path, $method, $parameters = array(), $multipart = false)
	{		
		$path = ltrim($path, '/');

		$parameters = ($parameters == null) ? array() : $parameters;
		$parameters['ext_session_id'] = key_exists('ext_session_id', $parameters) ? $parameters['ext_session_id'] : $this->getExtSessionId();

		if ($this->getExtSessionId() != $parameters['ext_session_id']){
			$this->saveCookie($parameters['ext_session_id']);
		}
		/*
		if (strtoupper($method) == 'POST' && in_array(rtrim($path,'/'),self::$PATHS_POST_MULTIPART))
		   $multipart = true;
	    else
	       $multipart = false;
		*/	    

		try
		{
			$result = parent::api($path, $method, $parameters, $multipart);
		}
		catch(FotiApiException $e)
		{			
			if (in_array($e->getCode(), $this->regenerationErrorCodes))
			{
				unset($parameters['access_token']);

				if ($this->autogenerateTokens)
				{
					$this->generateNewToken();
					$result = parent::api($path, $method, $parameters, $multipart);
				}
				else
					throw $e;
			}
			else
				throw $e;
		}

		return $result;
	}

	public function setEventHandler(callable $handler){
		$this->eventHandler = $handler;
	}

	
	// updates current library token	

	public function requestNewToken($appId, $secretKey)
	{
		$timeStamp 		= time();
		$uniqId			= uniqid (rand(), true);
		$stringToHash 	= $appId . "-" . $secretKey . "-" . $uniqId . "-" . $timeStamp;
		$hash 			= sha1($stringToHash);
		
		$fields = array(
			'application_id' => $appId,
			'unique_id'      => $uniqId,
			'timestamp'      => $timeStamp,
			'hash'           => $hash
		);

		$output = $this->processApiResponse($this->makeRequest(self::PATH_AUTH, 'POST' , $fields));		

		if (is_array($output))
			return $output;
		else
			return null;
	}

	public function generateNewToken()
	{
		$this->fire('generateNewToken',[]);

		$this->accessToken 	= null;
		$this->publicToken 	= null;

		$result = $this->requestNewToken($this->appId, $this->secretKey);
		$this->accessToken = $result['access_token'];
		$this->publicToken = $result['public_token'];

		$this->saveCookie();
		return $this->getAccessToken();
	}

	public function generateNewSession($extSessionId = null){
		if (empty($extSessionId))
			$extSessionId = md5(uniqid(rand(), true));

		$this->extSessionId = $extSessionId;
		$this->saveCookie($extSessionId);
	}
	
	public function getExtSessionId()
	{
		return $this->extSessionId;
	}
	
	public function setExtSessionId($extSessionId)
	{
		$this->extSessionId = $extSessionId;
	}
	
	
	private function getCryptSecretKey()
	{
		return substr(base64_encode($this->config['app_id'] . "-" . $this->config['secret_key']),0,16);
	}
	
	private function crypt($data){		
		return base64_encode(openssl_encrypt($data, 'AES-256-CFB', $this->getCryptSecretKey(), 0, $this->getIV()));
	}

	private function decrypt($data){		
		return rtrim(openssl_decrypt(base64_decode($data), 'AES-256-CFB', $this->getCryptSecretKey(), 0, $this->getIV()));
	}

	private function getIV(){
		return substr(md5($this->getCryptSecretKey()),0,16);
	}

	private function fillDataFromCookie($load_tokens = true)
	{
		//$this->fire('fillDataFromCookie');
		
		if (isset($_COOKIE[$this->cookieName]))
		{
			$cookieData = @unserialize(@urldecode($_COOKIE[$this->cookieName]));						

			if (is_array($cookieData))
			{
				if (!empty($cookieData['ext_session_id']))
					$this->setExtSessionId($cookieData['ext_session_id']);

				if ($load_tokens)
				{
					if (!empty($cookieData['access_token']))
						$this->setAccessToken($this->decrypt($cookieData['access_token']));

					if (!empty($cookieData['public_token']))
						$this->setPublicToken($cookieData['public_token']);					
				}
			}
		}		
	}

	public function saveCookie($extSessionId = null)
	{
		if ($this->disableCookie)
			return;

		$data = array('ext_session_id' 	=> $extSessionId ?? $this->getExtSessionId());

		if ($this->autogenerateTokens)
		{
			$data['access_token'] = $this->crypt($this->getAccessToken());
			$data['public_token'] = $this->getPublicToken();
		}

		setcookie($this->cookieName,
				  urlencode(serialize(($data))),
				  time() + $this->cookieExpiration,
				  $this->cookiePath,
				  $this->cookieDomain,
				  $this->cookieSecure,
				  $this->cookieHttponly);		

		//$this->fire('saveCookie',$data);
	}

	private function fire($event, $data = [])
	{
		if (!empty($this->eventHandler))
			call_user_func($this->eventHandler, $event, $data);
	}
}
